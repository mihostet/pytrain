import cython
import numpy as np
cimport numpy as np
ctypedef np.double_t FTYPE_t

@cython.boundscheck(False)
@cython.wraparound(False)
def concat_map(double[:] Ka, double[:,:] Ra, double[:,:,:] Ta, double[:] Kb, double[:,:] Rb, double[:,:,:] Tb):
    #---- Sum over S of Tb(I,S,K) * Ka(S)
    cdef np.ndarray[FTYPE_t, ndim=2, mode='c'] Es = np.zeros((6,6))
    for k in range(0,6):
        for i in range(0,6):
            for s in range(0,6):
                Es[i,k] += Tb[i,s,k] * Ka[s]

    #---- Sum over S of TB(I,J,S) * RA(S,K).
    cdef np.ndarray[FTYPE_t, ndim=3, mode='c'] Ts = np.zeros((6,6,6))
    for k in range(0,6):
        for i in range(0,6):
            for j in range(0,6):
                for s in range(0,6):
                    Ts[i,j,k] += Tb[i,j,s] * Ra[s,k]

    cdef np.ndarray[FTYPE_t, ndim=1, mode='c'] Kc = np.zeros(6)
    cdef np.ndarray[FTYPE_t, ndim=2, mode='c'] Rc = np.zeros((6,6))
    cdef np.ndarray[FTYPE_t, ndim=3, mode='c'] Tc = np.zeros((6,6,6))
    #---- Final values.
    for k in range(0,6):
        #---- Zero-order terms.
        Kc[k] = Kb[k]
        for s in range(0,6):
             Kc[k] += (Rb[k,s] + Es[k,s]) * Ka[s]

        #---- First-order terms.
        for j in range(0,6):
            for s in range(0,6):
                Rc[j,k] += (Rb[j,s] + 2.0 * Es[j,s]) * Ra[s,k]

        #---- Second-order terms.
        for j in range(k,6):
            for i in range(0,6):
                for s in range(0,6):
                    Tc[i,j,k] += (Rb[i,s] + 2.0 * Es[i,s]) * Ta[s,j,k] + Ts[i,s,j] * Ra[s,k]
                Tc[i,k,j] = Tc[i,j,k]
    return Kc, Rc, Tc