from typing import Optional, Tuple, Dict, List
from copy import deepcopy

from .twiss import *
from . import display_progress

    
class SvdOrbitCorrectionProcessor:
    def __init__(self, element_maps: Dict[str, TransportMap], 
                 monitors: List[str], 
                 correctors_h: List[str], correctors_v: List[str], 
                 ev_cut_h: float = 2e-4, ev_cut_v: float = 2e-4):
        self.element_maps = deepcopy(element_maps)
        self.monitors = deepcopy(monitors)
        self.correctors_h = deepcopy(correctors_h)
        self.correctors_v = deepcopy(correctors_v)
        self.ev_cut_h = ev_cut_h
        self.ev_cut_v = ev_cut_v
        self.reforbit_h = None
        self.rmatrix_h = None
        self.rmatrix_h_inv = None
        self.reforbit_v = None
        self.rmatrix_v = None
        self.rmatrix_v_inv = None
        self.calculate_response_matrices()

    def calculate_response_matrices(self):
        orbit = track_orbit(closed_orbit(oneturn_map(self.element_maps)), self.element_maps)
        self.reforbit_h = SvdOrbitCorrectionProcessor._extract_orbit(orbit, self.monitors, CanonicalVariable.X)
        self.rmatrix_h = SvdOrbitCorrectionProcessor._rmatrix(self.element_maps, self.monitors, self.correctors_h, CanonicalVariable.X)
        self.rmatrix_h_inv = np.linalg.pinv(self.rmatrix_h, rcond=self.ev_cut_h)

        self.reforbit_v = SvdOrbitCorrectionProcessor._extract_orbit(orbit, self.monitors, CanonicalVariable.Y)
        self.rmatrix_v = SvdOrbitCorrectionProcessor._rmatrix(self.element_maps, self.monitors, self.correctors_v, CanonicalVariable.Y)
        self.rmatrix_v_inv = np.linalg.pinv(self.rmatrix_v, rcond=self.ev_cut_v)
        
    def correct(self, orbit_h: np.ndarray, orbit_v: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
        orbit_err_h = orbit_h - self.reforbit_h
        correction_h = -orbit_err_h @ self.rmatrix_h_inv
        orbit_err_v = orbit_v - self.reforbit_v
        correction_v = -orbit_err_v @ self.rmatrix_v_inv
        return correction_h, correction_v

    @staticmethod
    def _extract_orbit(element_co: Dict[str, np.ndarray], monitors: List[str], plane: CanonicalVariable):
        return np.array([element_co[b] for b in monitors])[:, plane.value]

    @staticmethod
    def _rmatrix(element_maps: Dict[str, TransportMap], monitors: List[str], correctors: List[str], plane: CanonicalVariable, kick_scale: float = 1e-6):
        maps = deepcopy(element_maps)
        element_orbit = track_orbit(closed_orbit(oneturn_map(maps)), maps)
        reference = SvdOrbitCorrectionProcessor._extract_orbit(element_orbit, monitors, plane)

        rmatrix = []
        for c in correctors:
            maps[c].K[plane.P.value] += kick_scale
            response_orbit = track_orbit(closed_orbit(oneturn_map(maps)), maps)
            maps[c].K[plane.P.value] -= kick_scale
            response = SvdOrbitCorrectionProcessor._extract_orbit(response_orbit, monitors, plane) - reference
            rmatrix.append(response)
        rmatrix = np.vstack(rmatrix) / kick_scale
        
        return rmatrix

    def __call__(self, maps: Dict[int, Dict[str, TransportMap]]):
        orbits_h = []
        orbits_v = []
        z_ref = closed_orbit(oneturn_map(self.element_maps))
        for tm in display_progress(maps.values(), desc=f'Correcting mean orbit for all bunches ...'):
            orbit = track_orbit(closed_orbit(oneturn_map(tm), initial_guess=z_ref), tm)
            orbits_h.append(SvdOrbitCorrectionProcessor._extract_orbit(orbit, self.monitors, CanonicalVariable.X))
            orbits_v.append(SvdOrbitCorrectionProcessor._extract_orbit(orbit, self.monitors, CanonicalVariable.Y))
        mean_orbit_h = np.mean(np.vstack(orbits_h), axis=0)
        mean_orbit_v = np.mean(np.vstack(orbits_v), axis=0)
        correction_h, correction_v = self.correct(mean_orbit_h, mean_orbit_v)
        correction_maps = dict()
        for corrector, kick in zip(self.correctors_h, correction_h):
            correction_maps.setdefault(corrector, TransportMap.identity()).K[CanonicalVariable.PX.value] = kick
        for corrector, kick in zip(self.correctors_v, correction_v):
            correction_maps.setdefault(corrector, TransportMap.identity()).K[CanonicalVariable.PY.value] = kick
        
        new_maps = dict()
        for bunch, old_bunch_maps in maps.items():
            new_bunch_maps = dict()
            for element, tm in old_bunch_maps.items():
                new_bunch_maps[element] = tm
                if element in correction_maps:
                    new_bunch_maps[element+'_COD'] = correction_maps[element]
            maps[bunch] = new_bunch_maps
