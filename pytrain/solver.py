import logging
from typing import Dict

import numpy as np
from dataclasses import dataclass

from .beambeam import beambeam_map
from .twiss import *
from .machine import Machine, FillingScheme
from . import display_progress

LOGGER = logging.getLogger(__name__)


@dataclass(frozen=True)
class TrainSolution:
    machine: Machine
    maps_b1: Dict[int, Dict[str, TransportMap]]
    maps_b2: Dict[int, Dict[str, TransportMap]]
    oneturn_map_b1: Dict[int, TransportMap]
    oneturn_map_b2: Dict[int, TransportMap]
    z_b1: Dict[int, np.ndarray]
    z_b2: Dict[int, np.ndarray]
    closed_orbits_b1: Dict[int, Dict[str, np.ndarray]]
    closed_orbits_b2: Dict[int, Dict[str, np.ndarray]]

    def bunch_positions_b1(self, location: str) -> Tuple[np.ndarray, np.ndarray]:
        return TrainSolution._slice_bunch_orbits(self.closed_orbits_b1, self.machine.num_slots, location, idx=(0, 2))

    def bunch_positions_b2(self, location: str) -> Tuple[np.ndarray, np.ndarray]:
        return TrainSolution._slice_bunch_orbits(self.closed_orbits_b2, self.machine.num_slots, location, idx=(0, 2))

    def bunch_angles_b1(self, location: str) -> Tuple[np.ndarray, np.ndarray]:
        return TrainSolution._slice_bunch_orbits(self.closed_orbits_b1, self.machine.num_slots, location, idx=(1, 3))

    def bunch_angles_b2(self, location: str) -> Tuple[np.ndarray, np.ndarray]:
        return TrainSolution._slice_bunch_orbits(self.closed_orbits_b2, self.machine.num_slots, location, idx=(1, 3))

    @staticmethod
    def _slice_bunch_orbits(closed_orbits, num_slots, location, idx):
        co_bb_x = np.full((num_slots,), np.nan)
        co_bb_y = np.full((num_slots,), np.nan)
        for b, orbit in closed_orbits.items():
            co_bb_x[b] = orbit[location][idx[0]]
            co_bb_y[b] = orbit[location][idx[1]]
        return co_bb_x, co_bb_y

    def bunch_tunes_b1(self) -> Tuple[np.ndarray, np.ndarray]:
        return TrainSolution._calc_bunch_tunes(self.oneturn_map_b1, self.z_b1, self.machine.num_slots)

    def bunch_tunes_b2(self) -> Tuple[np.ndarray, np.ndarray]:
        return TrainSolution._calc_bunch_tunes(self.oneturn_map_b2, self.z_b2, self.machine.num_slots)

    @staticmethod
    def _calc_bunch_tunes(oneturn, z, num_slots):
        qx = np.full((num_slots,), np.nan)
        qy = np.full((num_slots,), np.nan)
        for b, bmap in oneturn.items():
            qx[b], qy[b] = tune(bmap, z[b])
        return qx, qy

    def bunch_chromas_b1(self) -> Tuple[np.ndarray, np.ndarray]:
        return TrainSolution._calc_bunch_chromas(self.oneturn_map_b1, self.z_b1, self.machine.num_slots)

    def bunch_chromas_b2(self) -> Tuple[np.ndarray, np.ndarray]:
        return TrainSolution._calc_bunch_chromas(self.oneturn_map_b2, self.z_b2, self.machine.num_slots)

    @staticmethod
    def _calc_bunch_chromas(oneturn, z, num_slots):
        qpx = np.full((num_slots,), np.nan)
        qpy = np.full((num_slots,), np.nan)
        for b, bmap in oneturn.items():
            qpx[b], qpy[b] = chroma(bmap, z[b])
        return qpx, qpy

    def bunch_twiss_b1(self) -> Dict[int, Dict[str, Twiss]]:
        if not hasattr(self, '_bunch_twiss_b1'):
            object.__setattr__(self, '_bunch_twiss_b1', TrainSolution._calc_bunch_twiss(self.maps_b1))
        return self._bunch_twiss_b1

    def bunch_twiss_b2(self) -> Dict[int, Dict[str, Twiss]]:
        if not hasattr(self, '_bunch_twiss_b2'):
            object.__setattr__(self, '_bunch_twiss_b2', TrainSolution._calc_bunch_twiss(self.maps_b2))
        return self._bunch_twiss_b2

    def bunch_betas_b1(self, location: str) -> Tuple[np.ndarray, np.ndarray]:
        return TrainSolution._slice_bunch_twiss(self.bunch_twiss_b1(), location, self.machine.num_slots, ('BETX', 'BETY'))

    def bunch_betas_b2(self, location: str) -> Tuple[np.ndarray, np.ndarray]:
        return TrainSolution._slice_bunch_twiss(self.bunch_twiss_b2(), location, self.machine.num_slots, ('BETX', 'BETY'))

    def bunch_dispersions_b1(self, location: str) -> Tuple[np.ndarray, np.ndarray]:
        return TrainSolution._slice_bunch_twiss(self.bunch_twiss_b1(), location, self.machine.num_slots, ('DX', 'DY'))

    def bunch_dispersions_b2(self, location: str) -> Tuple[np.ndarray, np.ndarray]:
        return TrainSolution._slice_bunch_twiss(self.bunch_twiss_b2(), location, self.machine.num_slots, ('DX', 'DY'))

    @staticmethod
    def _calc_bunch_twiss(maps):
        twisses = dict()
        for bunch, maps in display_progress(maps.items(), 'Twissing ...'):
            twisses[bunch] = twiss(maps)
        return twisses

    @staticmethod
    def _slice_bunch_twiss(twisses, location, num_slots, fields):
        if type(fields) is str:
            fields = (fields, )
        slices = {field: np.full((num_slots,), np.nan) for field in fields}
        for b, twiss in twisses.items():
            for field in fields:
                slices[field][b] = getattr(twiss[location], field)
        return tuple(slices.values())


def solve_train(machine: Machine, filling_scheme: FillingScheme,
                twiss_b1: Dict[str, Twiss], maps_b1: Dict[str, TransportMap],
                twiss_b2: Dict[str, Twiss], maps_b2: Dict[str, TransportMap],
                tolerance: float = 1e-9, max_iter: int = 5,
                maps_processor_b1 = None, maps_processor_b2 = None):
    oneturn_b1_nobb = oneturn_map(maps_b1)
    z1_nobb = closed_orbit(oneturn_b1_nobb)
    closed_orbit_b1_nobb = track_orbit(z1_nobb, maps_b1)

    oneturn_b2_nobb = oneturn_map(maps_b2)
    z2_nobb = closed_orbit(oneturn_b2_nobb)
    closed_orbit_b2_nobb = track_orbit(z2_nobb, maps_b2)

    maps_b1_bb = dict()
    maps_b2_bb = dict()
    closed_orbits_b1_bb = {b: closed_orbit_b1_nobb.copy() for b in filling_scheme.bunches_b1}
    closed_orbits_b2_bb = {b: closed_orbit_b2_nobb.copy() for b in filling_scheme.bunches_b2}
    oneturn_b1_bb = {b: oneturn_b1_nobb for b in filling_scheme.bunches_b1}
    oneturn_b2_bb = {b: oneturn_b2_nobb for b in filling_scheme.bunches_b2}
    z1_bb_old = {b: z1_nobb for b in filling_scheme.bunches_b1}
    z2_bb_old = {b: z2_nobb for b in filling_scheme.bunches_b2}
    z1_bb = {b: z1_nobb for b in filling_scheme.bunches_b1}
    z2_bb = {b: z2_nobb for b in filling_scheme.bunches_b2}

    it = 0
    max_err_b1 = np.nan
    max_err_b2 = np.nan
    for it in range(1, max_iter + 1):
        LOGGER.info(f'Starting iteration {it} - Beam 1')
        for b1 in display_progress(filling_scheme.bunches_b1, desc=f'Iteration {it} - Beam 1 - Generating beam-beam maps'):
            maps_bb = dict()
            LOGGER.debug(f'  Beam 1 - bunch {b1}')
            for element, sector_map in maps_b1.items():
                maps_bb[element] = sector_map
                if element not in machine.bb_positions:
                    continue
                b2 = (b1 + machine.bb_offset_slots[element] + machine.num_slots) % machine.num_slots
                if filling_scheme.intensity_b2[b2] == 0.0:
                    continue
                sig_x = np.sqrt(filling_scheme.emittance_b1h[b1] * twiss_b1[element].BETX / machine.gamma +
                                filling_scheme.emittance_b2h[b2] * twiss_b2[element].BETX / machine.gamma)
                sig_y = np.sqrt(filling_scheme.emittance_b1v[b1] * twiss_b1[element].BETY / machine.gamma +
                                filling_scheme.emittance_b2v[b2] * twiss_b2[element].BETY / machine.gamma)
                offset_x = closed_orbits_b2_bb[b2][element][0] - machine.bb_survey_separation[element].X
                offset_y = closed_orbits_b2_bb[b2][element][2] - machine.bb_survey_separation[element].Y
                bb_map = beambeam_map(orbit=closed_orbits_b1_bb[b1][element], intensity=filling_scheme.intensity_b2[b2],
                                      offset_x=offset_x, offset_y=offset_y, sig_x=sig_x, sig_y=sig_y,
                                      gamma=machine.gamma, particle=machine.particle_types.B2)
                maps_bb[element + '_BB'] = bb_map
            maps_b1_bb[b1] = maps_bb
        if maps_processor_b1 is not None:
            maps_processor_b1(maps_b1_bb)
        for b1 in display_progress(filling_scheme.bunches_b1, desc=f'Iteration {it} - Beam 1 - Solving closed orbits'):
            oneturn = oneturn_map(maps_b1_bb[b1])
            oneturn_b1_bb[b1] = oneturn
            z1_bb[b1] = closed_orbit(oneturn, initial_guess=z1_nobb)
            closed_orbits_b1_bb[b1] = track_orbit(z1_bb[b1], maps_b1_bb[b1])

        LOGGER.info(f'Starting iteration {it} - Beam 2')
        for b2 in display_progress(filling_scheme.bunches_b2, desc=f'Iteration {it} - Beam 2 - Generating beam-beam maps'):
            maps_bb = dict()
            LOGGER.debug(f'  Beam 2 - bunch {b2}')
            for element, sector_map in maps_b2.items():
                maps_bb[element] = sector_map
                if element not in machine.bb_positions:
                    continue
                b1 = (b2 - machine.bb_offset_slots[element] + machine.num_slots) % machine.num_slots
                if filling_scheme.intensity_b1[b1] == 0:
                    continue
                sig_x = np.sqrt(filling_scheme.emittance_b1h[b1] * twiss_b1[element].BETX / machine.gamma +
                                filling_scheme.emittance_b2h[b2] * twiss_b2[element].BETX / machine.gamma)
                sig_y = np.sqrt(filling_scheme.emittance_b1v[b1] * twiss_b1[element].BETY / machine.gamma +
                                filling_scheme.emittance_b2v[b2] * twiss_b2[element].BETY / machine.gamma)
                offset_x = closed_orbits_b1_bb[b1][element][0] + machine.bb_survey_separation[element].X
                offset_y = closed_orbits_b1_bb[b1][element][2] + machine.bb_survey_separation[element].Y
                bb_map = beambeam_map(orbit=closed_orbits_b2_bb[b2][element], intensity=filling_scheme.intensity_b1[b1],
                                      offset_x=offset_x, offset_y=offset_y, sig_x=sig_x, sig_y=sig_y,
                                      gamma=machine.gamma, particle=machine.particle_types.B1)
                maps_bb[element + '_BB'] = bb_map
            maps_b2_bb[b2] = maps_bb
        if maps_processor_b2 is not None:
            maps_processor_b2(maps_b2_bb)

        for b2 in display_progress(filling_scheme.bunches_b2, desc=f'Iteration {it} - Beam 2 - Solving closed orbits'):
            oneturn = oneturn_map(maps_b2_bb[b2])
            oneturn_b2_bb[b2] = oneturn
            z2_bb[b2] = closed_orbit(oneturn, initial_guess=z2_nobb)
            closed_orbits_b2_bb[b2] = track_orbit(z2_bb[b2], maps_b2_bb[b2])

        max_err_b1 = max((max(abs(z_new[:4] - z_old[:4]))
                          for z_new, z_old in zip(z1_bb.values(), z1_bb_old.values())))
        max_err_b2 = max((max(abs(z_new[:4] - z_old[:4]))
                          for z_new, z_old in zip(z2_bb.values(), z2_bb_old.values())))
        LOGGER.info(f'Iteration {it} done: max. closed-orbit error B1 / B2 = {max_err_b1:.4e} / {max_err_b2:.4e}')
        if max_err_b1 < tolerance and max_err_b2 < tolerance:
            break
        z1_bb_old = z1_bb.copy()
        z2_bb_old = z2_bb.copy()

    if max_err_b1 > tolerance or max_err_b2 > tolerance:
        LOGGER.warning(f'solve_train(): did not converge {it} iterations - '
                       f'final closed-orbit error B1 / B2 = {max_err_b1:.4e} / {max_err_b2:.4e}')

    return TrainSolution(
        machine=machine,
        oneturn_map_b1=oneturn_b1_bb,
        oneturn_map_b2=oneturn_b2_bb,
        maps_b1=maps_b1_bb,
        maps_b2=maps_b2_bb,
        z_b1=z1_bb,
        z_b2=z2_bb,
        closed_orbits_b1=closed_orbits_b1_bb,
        closed_orbits_b2=closed_orbits_b2_bb
    )
