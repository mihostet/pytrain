import logging
from typing import Tuple, Dict, Any, Iterator, List, Union, Literal, Set

import numpy as np
from cpymad import madx as cpymadx

from .machine import Survey, Machine
from .twiss import Twiss, TransportMap
import tempfile

LOGGER = logging.getLogger(__name__)


def fetch_git_optics_model(model_url) -> tempfile.TemporaryDirectory:
    from io import BytesIO
    from zipfile import ZipFile
    import requests
    import os
    from urllib.parse import urlparse
    model_url_parsed = urlparse(model_url)
    if model_url_parsed.scheme != 'gitlab+https':
        raise ValueError('Invalid repository URL - must start with gitlab+https://...')

    gitlab_host = model_url_parsed.netloc
    repo_path, ref = model_url_parsed.path.split('@')
    repo_name = repo_path.split('/')[-1]

    tmpdir = tempfile.TemporaryDirectory()
    zip_path = f'https://{gitlab_host}{repo_path}/-/archive/{ref}/{repo_name}-{ref}.zip'
    LOGGER.info(f'Downloading model from {zip_path}')
    resp = requests.get(zip_path)
    zipfile = ZipFile(BytesIO(resp.content))
    zipfile.extractall(tmpdir.name)
    os.rename(f'{tmpdir.name}/{repo_name}-{ref}', f'{tmpdir.name}/{repo_name}')
    return tmpdir


def cpymad_lhc_makethin(madx: cpymadx.Madx, slicefactor: int = 1):
    madx.globals.slicefactor = slicefactor
    madx.input('''
     select, flag=makethin, clear;

     select, flag=makethin, class=MB, slice=2;
     select, flag=makethin, class=MQ, slice=2 * slicefactor;

     select, flag=makethin, class=mqxa,  slice=16 * slicefactor;
     select, flag=makethin, class=mqxb,  slice=16 * slicefactor;

     select, flag=makethin, pattern=mbx\. ,   slice=4;
    !select, flag=makethin, pattern=mbxw\.,   slice=4;
     select, flag=makethin, pattern=mbrb\.,   slice=4;
     select, flag=makethin, pattern=mbrc\.,   slice=4;
     select, flag=makethin, pattern=mbrs\.,   slice=4;

     select, flag=makethin, pattern=mqwa\.,   slice=4 * slicefactor;
     select, flag=makethin, pattern=mqwb\.,   slice=4 * slicefactor;
     select, flag=makethin, pattern=mqy\.,    slice=4 * slicefactor;
     select, flag=makethin, pattern=mqm\.,    slice=4 * slicefactor;
     select, flag=makethin, pattern=mqmc\.,   slice=4 * slicefactor;
     select, flag=makethin, pattern=mqml\.,   slice=4 * slicefactor;
     select, flag=makethin, pattern=mqtlh\.,  slice=2 * slicefactor;
     select, flag=makethin, pattern=mqtli\.,  slice=2 * slicefactor;
     select, flag=makethin, pattern=mqt\.  ,  slice=2 * slicefactor; 

     use, sequence=lhcb1; makethin,sequence=lhcb1, style=teapot, makedipedge=true;
     use, sequence=lhcb2; makethin,sequence=lhcb2, style=teapot, makedipedge=true;
    ''')


def cpymad_lhc_install_bb_markers(madx: cpymadx.Madx, nparasitic: int = 45, 
                                  headon_ip: Union[Set[int], Literal['all']] = 'all', 
                                  parasitic_ip: Union[Set[int], Literal['all']] = 'all'):
    if headon_ip == 'all':
        headon_ip = {1,2,5,8}
    if parasitic_ip == 'all':
        parasitic_ip = {1,2,5,8}
    madx.globals.nparasitic = nparasitic
    madx.input(f'''
        INSTALL_SINGLE_BB_MARK(label,NN,where,origin) : macro = {{
          if (NN == 0) {{
            install,element=label,class=bbmarker,at=where,from=origin;
          }} else {{
            install,element=labelNN,class=bbmarker,at=where,from=origin;      
          }};
        }};

        INSTALL_BB_MARK(BIM) : macro = {{
          bbmarker: marker;
          seqedit,sequence=lhcBIM;

          where=1.e-9;
          {'exec INSTALL_SINGLE_BB_MARK(MKIP1,0,where,IP1);' if 1 in headon_ip else ''}
          {'exec INSTALL_SINGLE_BB_MARK(MKIP2,0,where,IP2);' if 2 in headon_ip else ''}
          {'exec INSTALL_SINGLE_BB_MARK(MKIP5,0,where,IP5);' if 5 in headon_ip else ''}
          {'exec INSTALL_SINGLE_BB_MARK(MKIP8,0,where,IP8);' if 8 in headon_ip else ''}

          {'n=1;while ( n <= nparasitic) {where=-n*b_h_dist;exec INSTALL_SINGLE_BB_MARK(MKIP1PL,$n,where,IP1.L1);n=n+1;};' if 1 in parasitic_ip else ''}
          {'n=1;while ( n <= nparasitic) {where= n*b_h_dist;exec INSTALL_SINGLE_BB_MARK(MKIP1PR,$n,where,IP1)   ;n=n+1;};' if 1 in parasitic_ip else ''}
          {'n=1;while ( n <= nparasitic) {where=-n*b_h_dist;exec INSTALL_SINGLE_BB_MARK(MKIP2PL,$n,where,IP2)   ;n=n+1;};' if 2 in parasitic_ip else ''}
          {'n=1;while ( n <= nparasitic) {where= n*b_h_dist;exec INSTALL_SINGLE_BB_MARK(MKIP2PR,$n,where,IP2)   ;n=n+1;};' if 2 in parasitic_ip else ''}
          {'n=1;while ( n <= nparasitic) {where=-n*b_h_dist;exec INSTALL_SINGLE_BB_MARK(MKIP5PL,$n,where,IP5)   ;n=n+1;};' if 5 in parasitic_ip else ''}
          {'n=1;while ( n <= nparasitic) {where= n*b_h_dist;exec INSTALL_SINGLE_BB_MARK(MKIP5PR,$n,where,IP5)   ;n=n+1;};' if 5 in parasitic_ip else ''}
          {'n=1;while ( n <= nparasitic) {where=-n*b_h_dist;exec INSTALL_SINGLE_BB_MARK(MKIP8PL,$n,where,IP8)   ;n=n+1;};' if 8 in parasitic_ip else ''}
          {'n=1;while ( n <= nparasitic) {where= n*b_h_dist;exec INSTALL_SINGLE_BB_MARK(MKIP8PR,$n,where,IP8)   ;n=n+1;};' if 8 in parasitic_ip else ''}
          endedit;
      }};

        b_t_dist = 25;
        b_h_dist := LHCLENGTH/HRF400 * 10./2. * b_t_dist / 25.;

        use, sequence=lhcb1;
        exec, INSTALL_BB_MARK(b1);
        use, sequence=lhcb2;
        exec, INSTALL_BB_MARK(b2);
        ''')


def cpymad_lhc_cycle(madx: cpymadx.Madx, start: str):
    madx.input(f'''
        seqedit,sequence=lhcb1;flatten;cycle,start={start};flatten;endedit;
        seqedit,sequence=lhcb2;flatten;cycle,start={start};flatten;endedit;
    ''')


def cpymad_lhc_match_tune_chroma(madx: cpymadx.Madx, qx_b1, qy_b1, qx_b2, qy_b2, qpx_b1, qpy_b1, qpx_b2, qpy_b2, tolerance=1e-10):
    madx.input(f'''
     use,sequence=lhcb1;
     dQpx.b1_sq={qpx_b1};
     dQpy.b1_sq={qpy_b1};
     match;
     global, q1={qx_b1}, q2={qy_b1};
     vary,   name=dQx.b1_sq, step=1.0E-4 ;
     vary,   name=dQy.b1_sq, step=1.0E-4 ;
     lmdif,  calls=100, tolerance={tolerance};
     endmatch;
     match,chrom;
     global, dq1={qpx_b1}, dq2={qpy_b1};
     global, q1={qx_b1}, q2={qy_b1};
     vary,   name=dQpx.b1_sq;
     vary,   name=dQpy.b1_sq;
     vary,   name=dQx.b1_sq, step=1.0E-4 ;
     vary,   name=dQy.b1_sq, step=1.0E-4 ;
     lmdif,  calls=500, tolerance={tolerance};
     endmatch;

     use,sequence=lhcb2;
     dQpx.b2_sq={qpx_b2};
     dQpy.b2_sq={qpy_b2};
     match;
     global, q1={qx_b2}, q2={qy_b2};
     vary,   name=dQx.b2_sq, step=1.0E-4 ;
     vary,   name=dQy.b2_sq, step=1.0E-4 ;
     lmdif,  calls=100, tolerance={tolerance};
     endmatch;
     match,chrom;
     global, dq1={qpx_b2}, dq2={qpy_b2};
     global, q1={qx_b2}, q2={qy_b2};
     vary,   name=dQpx.b2_sq;
     vary,   name=dQpy.b2_sq;
     vary,   name=dQx.b2_sq, step=1.0E-4 ;
     vary,   name=dQy.b2_sq, step=1.0E-4 ;
     lmdif,  calls=500, tolerance={tolerance};
     endmatch;

     use,sequence=lhcb1;
    ''')


def cpymad_select_bb_markers(madx: cpymadx.Madx,
                             extra_selections: Union[None, str, List[Union[str, Dict[str, str]]]] = None):
    madx.command.select(flag='sectormap', clear=True)
    madx.command.select(flag='sectormap', range='#e')
    madx.command.select(flag='sectormap', class_='marker', pattern='^MKIP.*')

    madx.command.select(flag='twiss', clear=True)
    madx.command.select(flag='twiss', class_='marker', pattern='^MKIP.*')

    madx.command.select(flag='survey', clear=True)
    madx.command.select(flag='survey', class_='marker', pattern='^MKIP.*')

    if extra_selections is not None:
        if type(extra_selections) is str:
            extra_selections = [extra_selections]
        for selection in extra_selections:
            if type(selection) is str:
                selection = {'pattern': selection}
            madx.command.select(flag='sectormap', **selection)
            madx.command.select(flag='twiss', **selection)
            madx.command.select(flag='survey', **selection)


def cpymad_table_entries(table: cpymadx.Table) -> Iterator[Dict[str, Any]]:
    for idx in table.selected_rows():
        yield table[idx]


def cpymad_table_survey(table: cpymadx.Table) -> Dict[str, Survey]:
    return {e['name'].replace(':1', '').upper(): Survey(X=e['X'], Y=e['Y'], Z=e['Z'])
            for e in cpymad_table_entries(table)}


def cpymad_table_twiss(table: cpymadx.Table) -> Dict[str, Twiss]:
    return {e['name'].replace(':1', '').upper(): Twiss(**{f: e[f] for f in Twiss.__dataclass_fields__ if f in e})
            for e in cpymad_table_entries(table)}


def cpymad_table_sectormaps(table: cpymadx.Table) -> Dict[str, TransportMap]:
    return {e['name'].upper(): TransportMap(
        K=np.array([e['k%d' % (i)] for i in range(1, 7)]),
        R=np.array([[e['r%d%d' % (i, j)]
                     for j in range(1, 7)]
                    for i in range(1, 7)]),
        T=np.array([[[e['t%d%d%d' % (i, j, k)]
                      for k in range(1, 7)]
                     for j in range(1, 7)]
                    for i in range(1, 7)])
    ) for e in cpymad_table_entries(table)}


def cpymad_generate_train_maps(madx, num_slots=3564, b1_seq='lhcb1', b2_seq='lhcb2',
                               extra_elements: Union[None, str, List[Union[str, Dict[str, str]]]] = None,
                               **kwargs) -> \
        Tuple[Machine, Dict[str, Twiss], Dict[str, Twiss], Dict[str, TransportMap], Dict[str, TransportMap]]:
    cpymad_select_bb_markers(madx, extra_selections=extra_elements)
    madx.use(b1_seq)
    madx.twiss(sectormap=True, sectorpure=True).selection()
    madx.survey()
    twiss_summary = dict(madx.table.twiss.summary)
    twiss_b1 = cpymad_table_twiss(madx.table.twiss)
    maps_b1 = cpymad_table_sectormaps(madx.table.sectortable)
    survey_b1 = cpymad_table_survey(madx.table.survey)

    madx.use(b2_seq)
    madx.twiss(sectormap=True, sectorpure=True).selection()
    madx.survey()
    twiss_b2 = cpymad_table_twiss(madx.table.twiss)
    maps_b2 = cpymad_table_sectormaps(madx.table.sectortable)
    survey_b2 = cpymad_table_survey(madx.table.survey)
    bbmk_positions = {bbmk: twiss.S for bbmk, twiss in twiss_b1.items() if bbmk.startswith('MKIP')}
    machine = Machine(length=float(twiss_summary['length']), num_slots=num_slots,
                      energy=float(twiss_summary['energy']), bb_positions=bbmk_positions, **kwargs)
    machine.set_survey_separation(survey_b1, survey_b2)

    return machine, twiss_b1, twiss_b2, maps_b1, maps_b2
