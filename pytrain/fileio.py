from typing import Dict

import pandas as pd

from .machine import *
from .twiss import *


def read_twiss_summary(file: str) -> pd.DataFrame:
    return pd.read_csv(file, sep='\\s+', nrows=47, usecols=[1, 3], index_col=0, header=None,
                       names=['variable', 'value']).value


def read_twiss_output(file: str) -> Dict[str, Twiss]:
    df = pd.read_csv(file, sep='\\s+', index_col=0, skiprows=52, header=None,
                     names=['NAME', 'S', 'X', 'BETX', 'ALFX', 'DX', 'Y', 'BETY', 'ALFY', 'DY', 'PX', 'PY', 'T', 'PT'])
    return {element: Twiss(**df.loc[element]) for element in df.index}


def read_sectormap_output(file: str) -> Dict[str, TransportMap]:
    imap = pd.read_csv(file, sep='\\s+', skiprows=0, index_col=0,
                       names=[
                           'POS', 'K1', 'K2', 'K3', 'K4', 'K5', 'K6',
                           'R11', 'R21', 'R31', 'R41', 'R51', 'R61', 'R12', 'R22', 'R32', 'R42', 'R52', 'R62', 'R13',
                           'R23', 'R33', 'R43', 'R53', 'R63', 'R14', 'R24', 'R34', 'R44', 'R54', 'R64', 'R15', 'R25',
                           'R35', 'R45', 'R55', 'R65', 'R16', 'R26', 'R36', 'R46', 'R56', 'R66',
                           'T111', 'T211', 'T311', 'T411', 'T511', 'T611', 'T121', 'T221', 'T321', 'T421', 'T521',
                           'T621', 'T131', 'T231', 'T331', 'T431', 'T531', 'T631', 'T141', 'T241', 'T341', 'T441',
                           'T541', 'T641', 'T151', 'T251', 'T351', 'T451', 'T551', 'T651', 'T161', 'T261', 'T361',
                           'T461', 'T561', 'T661', 'T112', 'T212', 'T312', 'T412', 'T512', 'T612', 'T122', 'T222',
                           'T322', 'T422', 'T522', 'T622', 'T132', 'T232', 'T332', 'T432', 'T532', 'T632', 'T142',
                           'T242', 'T342', 'T442', 'T542', 'T642', 'T152', 'T252', 'T352', 'T452', 'T552', 'T652',
                           'T162', 'T262', 'T362', 'T462', 'T562', 'T662', 'T113', 'T213', 'T313', 'T413', 'T513',
                           'T613', 'T123', 'T223', 'T323', 'T423', 'T523', 'T623', 'T133', 'T233', 'T333', 'T433',
                           'T533', 'T633', 'T143', 'T243', 'T343', 'T443', 'T543', 'T643', 'T153', 'T253', 'T353',
                           'T453', 'T553', 'T653', 'T163', 'T263', 'T363', 'T463', 'T563', 'T663', 'T114', 'T214',
                           'T314', 'T414', 'T514', 'T614', 'T124', 'T224', 'T324', 'T424', 'T524', 'T624', 'T134',
                           'T234', 'T334', 'T434', 'T534', 'T634', 'T144', 'T244', 'T344', 'T444', 'T544', 'T644',
                           'T154', 'T254', 'T354', 'T454', 'T554', 'T654', 'T164', 'T264', 'T364', 'T464', 'T564',
                           'T664', 'T115', 'T215', 'T315', 'T415', 'T515', 'T615', 'T125', 'T225', 'T325', 'T425',
                           'T525', 'T625', 'T135', 'T235', 'T335', 'T435', 'T535', 'T635', 'T145', 'T245', 'T345',
                           'T445', 'T545', 'T645', 'T155', 'T255', 'T355', 'T455', 'T555', 'T655', 'T165', 'T265',
                           'T365', 'T465', 'T565', 'T665', 'T116', 'T216', 'T316', 'T416', 'T516', 'T616', 'T126',
                           'T226', 'T326', 'T426', 'T526', 'T626', 'T136', 'T236', 'T336', 'T436', 'T536', 'T636',
                           'T146', 'T246', 'T346', 'T446', 'T546', 'T646', 'T156', 'T256', 'T356', 'T456', 'T556',
                           'T656', 'T166', 'T266', 'T366', 'T466', 'T566', 'T666'])

    return {element: TransportMap(
        K=np.array([imap['K%d' % (i)][element] for i in range(1, 7)]),
        R=np.array([[imap['R%d%d' % (i, j)][element]
                     for j in range(1, 7)]
                    for i in range(1, 7)]),
        T=np.array([[[imap['T%d%d%d' % (i, j, k)][element]
                      for k in range(1, 7)]
                     for j in range(1, 7)]
                    for i in range(1, 7)])
    ) for element in imap.index}


def read_survey(file: str) -> Dict[str, Survey]:
    df = pd.read_csv(file, sep='\\s+', index_col=0, skiprows=8, header=None,
                       names=['NAME', 'X', 'Y', 'Z'])
    return {element: Survey(**df.loc[element]) for element in df.index}


def read_train_files(prefix: str, num_slots=3564) -> \
        Tuple[Machine, Dict[str, Twiss], Dict[str, Twiss], Dict[str, TransportMap], Dict[str, TransportMap]]:
    survey_b1 = read_survey(f'{prefix}/train.surf')
    survey_b2 = read_survey(f'{prefix}/train.surb')
    twiss_summary = read_twiss_summary(f'{prefix}/train.optf')
    twiss_b1 = read_twiss_output(f'{prefix}/train.optf')
    twiss_b2 = read_twiss_output(f'{prefix}/train.optb')
    maps_b1 = read_sectormap_output(f'{prefix}/train.manf')
    maps_b2 = read_sectormap_output(f'{prefix}/train.manb')
    bbmk_positions = {bbmk: twiss.S for bbmk, twiss in twiss_b1.items() if bbmk.startswith('MKIP')}
    machine = Machine(length=float(twiss_summary['LENGTH']), num_slots=num_slots,
                      energy=float(twiss_summary['ENERGY']), bb_positions=bbmk_positions)
    machine.set_survey_separation(survey_b1, survey_b2)
    return machine, twiss_b1, twiss_b2, maps_b1, maps_b2
