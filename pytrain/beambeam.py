import numpy as np
from scipy.special import wofz

from .particles import ParticleType
from .twiss import TransportMap


def werrf(x, y):
    z = wofz(x + 1j * y)
    return z.real, z.imag


# exp(-EXP_LIMIT) < float precision
EXP_LIMIT = 150.0


def beambeam_map(orbit: np.ndarray, intensity: float,
                 offset_x: float, offset_y: float, sig_x: float, sig_y: float,
                 gamma: float, particle: ParticleType) -> TransportMap:
    fk = 2 * particle.radius * intensity / gamma
    if sig_x == sig_y:
        # avoid a special case...
        sig_x = sig_y + 0.1e-12

    sx2 = sig_x * sig_x
    sy2 = sig_y * sig_y

    xs = orbit[0] - offset_x
    ys = orbit[2] - offset_y

    r2 = 2 * (sx2 - sy2)
    if sx2 > sy2:
        r = np.sqrt(r2)
        rk = fk * np.sqrt(np.pi) / r
        xr = abs(xs) / r
        yr = abs(ys) / r
        crx, cry = werrf(xr, yr)
        tk = (xs * xs / sx2 + ys * ys / sy2) / 2.0
        if tk > EXP_LIMIT:
            exk = 0.0
            cbx = 0.0
            cby = 0.0
        else:
            exk = np.exp(-tk)
            xb = (sig_y / sig_x) * xr
            yb = (sig_x / sig_y) * yr
            cbx, cby = werrf(xb, yb)

    else:
        r = np.sqrt(-r2)
        rk = fk * np.sqrt(np.pi) / r
        xr = abs(xs) / r
        yr = abs(ys) / r
        cry, crx = werrf(yr, xr)
        tk = (xs * xs / sx2 + ys * ys / sy2) / 2.0
        if tk > EXP_LIMIT:
            exk = 0.0
            cbx = 0.0
            cby = 0.0
        else:
            exk = np.exp(-tk)
            xb = (sig_y / sig_x) * xr
            yb = (sig_x / sig_y) * yr
            cby, cbx = werrf(yb, xb)

    # kicks
    phix = rk * (cry - exk * cby) * np.sign(xs)
    phiy = rk * (crx - exk * cbx) * np.sign(ys)
    K = np.zeros(6)
    K[1] = phix
    K[3] = phiy

    # first-order
    phixx = (2.0 / r2) * (- (xs * phix + ys * phiy) + fk * (1.0 - (sig_y / sig_x) * exk))
    phixy = (2.0 / r2) * (- (xs * phiy - ys * phix))
    phiyy = (2.0 / r2) * (+ (xs * phix + ys * phiy) - fk * (1.0 - (sig_x / sig_y) * exk))
    R = np.eye(6)
    R[1, 0] = phixx
    R[1, 2] = phixy
    R[3, 0] = phixy
    R[3, 2] = phiyy

    # second-order
    phixxx = (- phix - (xs * phixx + ys * phixy) + fk * xs * sig_y * exk / sig_x ** 3) / r2
    phixxy = (- phiy - (xs * phixy - ys * phixx)) / r2
    phixyy = (+ phix - (xs * phiyy - ys * phixy)) / r2
    phiyyy = (+ phiy + (xs * phixy + ys * phiyy) - fk * ys * sig_x * exk / sig_y ** 3) / r2
    T = np.zeros((6, 6, 6))
    T[1, 0, 0] = phixxx
    T[1, 0, 2] = phixxy
    T[1, 2, 0] = phixxy
    T[3, 0, 0] = phixxy
    T[1, 2, 2] = phixyy
    T[3, 0, 2] = phixyy
    T[3, 2, 0] = phixyy
    T[3, 2, 2] = phiyyy

    # kicks include the feed-down from first and second order - get rid of it
    R -= 2 * (T @ orbit)
    K -= (R @ orbit + T @ orbit @ orbit) - orbit

    return TransportMap(K, R, T)
