import typing
from dataclasses import dataclass
from typing import Dict
import logging

import numpy as np
from .particles import ParticleType, PROTON

LOGGER = logging.getLogger(__name__)

ParticleTypes = typing.NamedTuple('ParticleTypes', [('B1', ParticleType), ('B2', ParticleType)])
Survey = typing.NamedTuple('Survey', [('X', float), ('Y', float), ('Z', float)])
Separation = typing.NamedTuple('Separation', [('X', float), ('Y', float)])


class Machine:
    def __init__(self, length: float, num_slots: int, energy: float, bb_positions: Dict[str, float],
                 particle_types: ParticleTypes = (PROTON, PROTON), bunch0_ref='MKIP1'):
        self.particle_types = ParticleTypes(*particle_types)
        self.length = length
        self.num_slots = num_slots
        self.energy = energy
        self.gamma = energy * self.particle_types.B1.charge / self.particle_types.B1.mass
        self._init_beam_beam_schedule(bb_positions, bunch0_ref)

    def _init_beam_beam_schedule(self, bb_positions: Dict[str, float], bunch0_ref: str):
        s_ref = bb_positions[bunch0_ref]
        slot_len = self.length / self.num_slots

        bb_markers = [mk for mk in bb_positions if self.associated_ip_marker(mk) is not None]
        self.bb_offset_slots = dict()
        self.bb_positions = dict()
        for bbmk in bb_markers:
            s = bb_positions[bbmk] - s_ref
            offset_slots_frac = 2 * (s / slot_len)
            offset_slots = round(offset_slots_frac)
            if abs(offset_slots - offset_slots_frac) > 0.1:
                LOGGER.warning(f'{bbmk} does not collide at a full bunch slot -> not a BB interaction point. Ignored.')
            else:
                self.bb_offset_slots[bbmk] = (offset_slots + self.num_slots) % self.num_slots
                self.bb_positions[bbmk] = bb_positions[bbmk]
        self.bb_survey_separation = {element: Separation(0.0, 0.0) for element in self.bb_positions}

    @classmethod
    def associated_ip_marker(cls, bb_marker_name):
        if bb_marker_name.startswith('MKIP'):
            return bb_marker_name[:5]
        else:
            return None

    def set_survey_separation(self, survey_b1: Dict[str, Survey], survey_b2: Dict[str, Survey]):
        for marker in self.bb_positions:
            ip_marker = self.associated_ip_marker(marker)
            s1 = Survey(*(s_mk - s_ip for s_mk, s_ip in zip(survey_b1[marker], survey_b1[ip_marker])))
            s2 = Survey(*(s_mk - s_ip for s_mk, s_ip in zip(survey_b2[marker], survey_b2[ip_marker])))
            angle = np.arctan2(s1.Z - s2.Z, s1.X - s2.X) - 2 * np.pi * self.bb_positions[marker] / self.length
            while abs(angle) > np.pi:
                angle -= np.sign(angle) * 2 * np.pi
            xsign = 1 if abs(angle) <= (np.pi / 2.0) else -1
            sep_x = np.sqrt((s1.X - s2.X) ** 2 + (s1.Z - s2.Z) ** 2) * xsign
            sep_y = s1.Y - s2.Y
            self.bb_survey_separation[marker] = Separation(sep_x, sep_y)


@dataclass
class FillingScheme:
    intensity_b1: np.ndarray
    intensity_b2: np.ndarray
    emittance_b1h: np.ndarray
    emittance_b1v: np.ndarray
    emittance_b2h: np.ndarray
    emittance_b2v: np.ndarray

    @property
    def bunches_b1(self):
        return {b for b, intensity in enumerate(self.intensity_b1) if intensity > 0.0}

    @property
    def bunches_b2(self):
        return {b for b, intensity in enumerate(self.intensity_b2) if intensity > 0.0}
