__version__ = "0.1.0"

try:
    from tqdm.auto import tqdm as display_progress
except ImportError:  # no tqdm - no progress bars.
    def display_progress(it, *args, **kwargs):
        return it