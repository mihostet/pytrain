import functools
import logging
from dataclasses import dataclass
from typing import Optional, Tuple, Dict
from enum import Enum

import numpy as np
from scipy import linalg

# noinspection PyUnresolvedReferences
from .operations import concat_map

LOGGER = logging.getLogger(__name__)

class CanonicalVariable(Enum):
    X = 0
    PX = 1
    Y = 2
    PY = 3
    T = 4
    PT = 5
    
    @property
    def P(self):
        if self == CanonicalVariable.X:
            return CanonicalVariable.PX
        elif self == CanonicalVariable.Y:
            return CanonicalVariable.PY
        elif self == CanonicalVariable.T:
            return CanonicalVariable.PT
        else:
            raise ValueError(f'{self} is not a position variable (X, Y, T)')


@dataclass
class TransportMap:
    K: np.ndarray
    R: np.ndarray
    T: np.ndarray

    def J(self, z):
        return self.R + 2 * self.T @ z

    def apply(self, z):
        return self.K + self.R @ z + self.T @ z @ z

    def concat(self, other):
        Kc, Rc, Tc = concat_map(self.K, self.R, self.T, other.K, other.R, other.T)
        return TransportMap(Kc, Rc, Tc)

    def __matmul__(self, other):
        if isinstance(other, TransportMap):
            return self.concat(other)
        else:
            return self.apply(other)

    @classmethod
    def identity(cls):
        return cls(K=np.zeros((6,)), R=np.eye(6), T=np.zeros((6, 6, 6)))

    def __eq__(self, other):
        return np.all(self.K == other.K) and np.all(self.R == other.R) and np.all(self.T == other.T)


@dataclass(frozen=True)
class Twiss:
    S: float
    BETX: float
    BETY: float
    X: float = np.nan
    Y: float = np.nan
    PX: float = np.nan
    PY: float = np.nan
    T: float = np.nan
    PT: float = np.nan
    DX: float = np.nan
    DY: float = np.nan
    MUX: float = np.nan
    MUY: float = np.nan
    ALFX: float = np.nan
    ALFY: float = np.nan


def oneturn_map(maps: Dict[str, TransportMap]) -> TransportMap:
    return functools.reduce(TransportMap.concat, maps.values())


def closed_orbit(oneturn_map: TransportMap, initial_guess: Optional[np.ndarray] = None, tolerance: float = 1e-12,
                 max_iter: int = 1000):
    if initial_guess is None:
        z = np.zeros((6,))
    else:
        z = initial_guess.copy()

    i = 0
    z1 = None
    # find starting point (x,px,y,py,t,pt) on CO
    for i in range(1, max_iter + 1):
        z1 = oneturn_map @ z
        J = oneturn_map.J(z1)
        z[:4] -= (linalg.inv(J[:4, :4] - np.eye(4)) @ (z1 - z)[:4])
        # print("iteration %d >> %s\n   diff=%s" %(i, str(z), str(z1-z)))
        if all(abs(z1[:4] - z[:4]) < tolerance):
            break
    if i == max_iter:
        LOGGER.warning(f'closed_orbit(): did not converge {i} iterations - final error: {abs(z1 - z)}')
    return z


def track_orbit(orbit: np.ndarray, element_maps: Dict[str, TransportMap]) -> Dict[str, np.ndarray]:
    element_orbits = dict()
    for element, map in element_maps.items():
        orbit = map @ orbit
        element_orbits[element] = orbit
    return element_orbits


def tune(oneturn_map: TransportMap, orbit: Optional[np.ndarray] = None) -> Tuple[float, float]:
    if orbit is None:
        J = oneturn_map.R
    else:
        J = oneturn_map.J(orbit)
    q = np.arccos(np.real(linalg.eig(J)[0])) / (2 * np.pi)
    return q[1], q[3]


def chroma(oneturn_map: TransportMap, orbit: Optional[np.ndarray] = None) -> Tuple[float, float]:
    if orbit is None:
        J = oneturn_map.R
    else:
        J = oneturn_map.J(orbit)
    T = oneturn_map.T

    disp0 = linalg.inv(J[:4, :4] - np.eye(4)) @ (-J[:4, 5])

    sinmux = np.sqrt(-J[0, 1] * J[1, 0] - (J[0, 0] - J[1, 1]) ** 2 / 4.0) * np.sign(J[0, 1])
    sinmuy = np.sqrt(-J[2, 3] * J[3, 2] - (J[2, 2] - J[3, 3]) ** 2 / 4.0) * np.sign(J[2, 3])

    sx = T[0, 0, 5] + T[1, 1, 5]
    sy = T[2, 2, 5] + T[3, 3, 5]

    for i in range(0, 4):
        sx += (T[0, 0, i] + T[1, 1, i]) * disp0[i]
        sy += (T[2, 2, i] + T[3, 3, i]) * disp0[i]

    dqx = -sx / (2 * np.pi * sinmux)
    dqy = -sy / (2 * np.pi * sinmuy)

    return dqx, dqy

def twiss(element_maps: Dict[str, TransportMap]) -> Dict[str, Twiss]:
    # twiss code inspired by MAD-X
    # this is assuming an un-coupled (block diagonal) R
    # TODO: handle correctly the coupled case and block-diagonalize R
    oneturn = oneturn_map(element_maps)
    orbit = closed_orbit(oneturn)
    J = oneturn.J(orbit)
    
    disp = linalg.inv(J[:4, :4] - np.eye(4)) @ (-J[:4, 5])

    cosmux = (J[0, 0] + J[1, 1]) / 2.0
    sinmux = np.sqrt(-J[0, 1] * J[1, 0] - (J[0, 0] - J[1, 1]) ** 2 / 4.0) * np.sign(J[0, 1])
    cosmuy = (J[2, 2] + J[3, 3]) / 2.0
    sinmuy = np.sqrt(-J[2, 3] * J[3, 2] - (J[2, 2] - J[3, 3]) ** 2 / 4.0) * np.sign(J[2, 3])
    betx = J[0, 1] / sinmux
    alfx = (J[0, 0] - J[1, 1]) / (2.0 * sinmux)
    bety = J[2, 3] / sinmuy
    alfy = (J[2, 2] - J[3, 3]) / (2.0 * sinmuy)
#    we can not calculate phase advances correctly, as the phase avance between two sectormaps
#    can be very well > 2*pi, and the integer part is lost ...
#    for the moment we don't invent anything, but just skip the calculaton
#    mux = 0.0
#    muy = 0.0
    
    elements_twiss = dict()
    for element, map in element_maps.items():
        J = map.J(orbit)
        orbit = map @ orbit

        dt = np.hstack([disp, [0, 1]])
        dt = J @ dt
        disp = dt[:4]

        adet = J[0,0] * J[1,1] - J[0,1] * J[1,0]
        # horizontal motion (mode 1)
        tempb = J[0,0] * betx - J[0,1] * alfx
        tempa = J[1,0] * betx - J[1,1] * alfx
        alfx = - (tempa * tempb + J[0,1] * J[1,1]) / (adet * betx)
        betx =   (tempb * tempb + J[0,1] * J[0,1]) / (adet * betx)
#        if abs(J[0, 1]) > 1e-12:
#            mux += np.arctan2(J[0, 1], tempb) / (2 * np.pi)
        # vertical motion (mode 2)
        tempb = J[2,2] * bety - J[2,3] * alfy
        tempa = J[3,2] * bety - J[3,3] * alfy
        alfy = - (tempa * tempb + J[2,3] * J[3,3]) / (adet * bety)
        bety =   (tempb * tempb + J[2,3] * J[2,3]) / (adet * bety)
#        if abs(J[2, 3]) > 1e-12:
#            muy += np.arctan2(J[2, 3], tempb) / (2 * np.pi)
        
        elements_twiss[element] = Twiss(S=np.nan, ALFX=alfx, ALFY=alfy, BETX=betx, BETY=bety, 
                                        X=orbit[0], PX=orbit[1], Y=orbit[2], PY=orbit[3], 
                                        DX=disp[0], DY=disp[2])
    return elements_twiss
