from dataclasses import dataclass, field
import numpy as np
from scipy import constants


@dataclass
class ParticleType:
    charge: int  # e
    mass: float  # GeV
    radius: float = field(init=False)

    def __post_init__(self):
        self.radius = self.charge * constants.e / (4 * np.pi * constants.epsilon_0 * self.mass * 1e9)


PROTON = ParticleType(charge=+1, mass=0.93827231)
ELECTRON = ParticleType(charge=-1, mass=0.51099906e-03)
PB82 = ParticleType(charge=+82, mass=193.687120)
