"""
setup.py for pytrain.

For reference see
https://packaging.python.org/guides/distributing-packages-using-setuptools/

"""
from pathlib import Path

import numpy
from setuptools import setup, find_packages
from Cython.Build import cythonize

HERE = Path(__file__).parent.absolute()
with (HERE / 'README.md').open('rt', encoding='utf-8') as fh:
    LONG_DESCRIPTION = fh.read().strip()


REQUIREMENTS: dict = {
    'core': [
        'numpy',
        'scipy',
        'tqdm',
        'Cython'
    ],
    'test': [
        'pytest',
    ],
    'dev': [
        'matplotlib'
    ],
    'doc': [
        'sphinx',
        'acc-py-sphinx',
    ],
    'cpymad': [
        'cpymad'
    ],
    'fileio': [
        'pandas'
    ]
}


setup(
    name='pytrain',
    version="0.1.0",

    author='Michi',
    author_email='michi.hostettler@cern.ch',
    description='Pure-Python TRAIN',
    long_description=LONG_DESCRIPTION,
    long_description_content_type='text/markdown',
    url='',

    packages=find_packages(),
    python_requires='~=3.7',
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],

    install_requires=REQUIREMENTS['core'],
    extras_require={
        **REQUIREMENTS,
        # The 'dev' extra is the union of 'test' and 'doc', with an option
        # to have explicit development dependencies listed.
        'dev': [req
                for extra in ['dev', 'test', 'doc']
                for req in REQUIREMENTS.get(extra, [])],
        # The 'all' extra is the union of all requirements.
        'all': [req for reqs in REQUIREMENTS.values() for req in reqs],
    },

    ext_modules=cythonize('pytrain/operations.pyx'),
    include_dirs=[numpy.get_include()]
)
