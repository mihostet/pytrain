# pytrain

A pure-python re-implementation of TRAIN. Slower than the original, but more extensible ... a toolkit to solve
self-consistent orbits under the presence of BBLR interactions, and to analyze 1-turn maps (closed orbit,
tune, chroma, ...)

Example usage (see also example.ipynb and example-cpymad.ipynb):
```python
from pytrain.fileio import read_train_files
from pytrain.twiss import *
from pytrain.machine import *
from pytrain.solver import solve_train

# read train input files
machine, twiss_b1, twiss_b2, maps_b1, maps_b2 = read_train_files('train-output')
# alternatively, use cpymad directly to generate the objects - see example-cpymad.ipynb

# 1-turn map, closed orbit and tunes without BB
oneturn_b1 = oneturn_map(maps_b1)

z1 = closed_orbit(oneturn_b1)
orbit1 = track_orbit(z1, maps_b1)

print(tune(oneturn_b1, z1))
print(chroma(oneturn_b1, z1))


# construct a filling scheme
# all the parameters need to be numpy arrays
filling_scheme = FillingScheme(int_b1, int_b2, emit_b1x, emit_b1y, emit_b2x, emit_b2y)

# solve self-consistent orbits with BBLR interactions
# this will take a while ...
result = solve_train(machine, filling_scheme, twiss_b1, maps_b1, twiss_b2, maps_b2)

# the result contains the final element-by-element maps for all bunches, one-turn maps, and closed orbits

# bunch-by-bunch closed orbit at any element
co_b1_x, co_b1_y = result.bunch_positions_b1('MKIP5')
co_b2_x, co_b2_y = result.bunch_positions_b2('MKIP5')
co_b1_px, co_b1_py = result.bunch_angles_b1('MKIP5')
co_b2_px, co_b2_py = result.bunch_angles_b2('MKIP5')

# bunch-by-bunch tunes and chroma
qx1, qy1 = result.bunch_tunes_b1()
qx2, qy2 = result.bunch_tunes_b2()
qpx1, qpy1 = result.bunch_chromas_b1()
qpx2, qpy2 = result.bunch_chromas_b2()
```

## SWAN usage

The recommended way to use pytrain on SWAN is to use Phil Elson's [swan-run-in-venv](https://gitlab.cern.ch/pelson/swan-run-in-venv) script:
```
!curl -s https://gitlab.cern.ch/pelson/swan-run-in-venv/-/raw/master/run_in_venv.py -o .run_in_venv
%run .run_in_venv pytrain-venv -m pip install --upgrade git+https://gitlab.cern.ch/mihostet/pytrain.git@pure-python
```
then use pytrain as usual. In case, `cpymad` can be installed in the same way into the venv.